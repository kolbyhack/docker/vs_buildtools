# escape=`

# Use the latest Windows Server Core image with .NET Framework 4.8.
FROM mcr.microsoft.com/dotnet/framework/sdk:4.8-windowsservercore-ltsc2019

# Restore the default Windows shell for correct batch processing.
SHELL ["cmd", "/S", "/C"]

# Install chocolatey
ADD https://chocolatey.org/install.ps1 C:\TEMP\chocolatey_install.ps1

RUN "%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" `
    -NoProfile `
    -InputFormat None `
    -ExecutionPolicy Bypass `
    C:\TEMP\chocolatey_install.ps1

RUN SETX PATH "%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

# Install chocolatey packages
COPY chocolatey.config C:\TEMP\chocolatey.config
RUN choco feature enable -n allowGlobalConfirmation && `
    choco install C:\TEMP\chocolatey.config

# Define the entry point for the docker container.
# This entry point starts the developer command prompt and launches the PowerShell shell.
ENTRYPOINT ["C:\\BuildTools\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]
